#pragma once
#include <iostream>
#include <list>
#include <vector>
#include <ctime>
#include <random>
#include <chrono>
#include <fstream>
#include <iomanip>
#include "User.h"
#include "UserShort.h"

typedef std::vector<User> Users;			//	type for storing users info
typedef std::vector<UserShort> UsersShort;	//	type for storing indexes and time_reg of found users

class UserManager
{
	Users m_users;							//	vector for storing users info
	UsersShort m_ids;						//	vector for storing indexes and time_reg of found users
	std::default_random_engine m_generator;	//	generator for generating random user base
	std::string m_filename;					//	filename for storing user base
	size_t m_limit;							//	user limit per one page

public:
	UserManager(const std::string& filename = "users.dat");	//	default constructor
	void GenerateUser(int id, User* user);					//	generate user
	void GenerateUsers(int n_users, bool clear = false);	//	generate number of users
	bool SaveUsers();										//	save user base in file. return true if successful
	bool LoadUsers();										//	load user base from file. return true if successful
	size_t FindUsers(const User& user);						//	find users by parameters, -1 if parameter not needed
															//	return number of users in base
	size_t GetUsersCount(void);								//	return number of users in base
	size_t GetFoundUsers(Users* users);						//	get all found users. return number of found users
	size_t GetFoundUsersCount(void);						//	return number of found users
	size_t GetPage(Users* users, unsigned page);			//	get found users for given page
	void SetPageLimit(unsigned limit);						//	set limit for users per page
	size_t GetPageLimit(void);								//	return limit for users per page
	size_t GetPagesNumber(void);							//	get pages number for found users
	void ClearFoundUsers(void);								//	clears vector for storing indexes of found users
	size_t PrintFoundUsers(void);							//	print all found users, return number of found users
	size_t PrintPage(unsigned page);						//	print given page with found users
	void PrintUser(const User* user);						//	print given user 
	bool AddUser(const User* user);							//	add user
	void SortFoundUsersByTimeReg(void);						//	sort found users by time reg
	void SortAllUsersByTimeReg(void);						//	sort all users by time reg
};

