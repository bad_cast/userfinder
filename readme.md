# Usage
Just compile and start program.
There will be 6 available options after start:
`1. Generate base - generates new base with given number of users.`
`2. Save base - save base to file (default users.dat).`
`3. Load base - load base from file (default users.dat).`
`4. Search user - search users by one or multiple parameters. If parameter not requiered, just type '-1'.`
`5. Set limit per page - set limit per page for printing users.`
`6. Add user - adds user with given parameters.`
When users found, you can switch between pages with arrows left or right. Or press ESC to exit in main menu.
# Notice
- I used vectors because they are good in given task. First vector holds info about all users, second vector holds info about their indexes and time_reg field when searching.
- Average search in base with 10,000,000 users is executing in ~25-40 msec, if number of found users is not very large (<100,000). Tested on Ryzen 2600. Intel i7 CPUs should perform a little better I guess.
- I tried to use stl where I can.
- Not implemented some functions because of time (edit user, delete user, print more human friendly cities, genders, etc).
- Result may be better if using threads. Not implemented because of time.
- First I wanted to realize two apps: one holds base and receives requests, second is cgi for apache for forming requests. Not implemented because of time.
- There may be some bugs, but search is working good.
- I developed in Visual Studio, but in Linux should also compile and link. See gcc.bat