#pragma once

#include <algorithm>

enum Gender {
	UNKNOWN = -1, MALE, FEMALE, LESBIAN, GAY, BISEXUAL, TRANSGENDER, AGENDER, BIGENDER, ANDROGYNE, FTM, MTF, FLUID, HELICOPTER, LAST
};

class User {
public:
	int id;				
	time_t birthday;	// birthday date
	int gender;			// gender
	int city_id;		// city id
	time_t time_reg;	// reg date

	bool operator==(const User& other) const;	//	equality operator, used for checking of users' equality 
	bool operator<(const User& other) const;	//	lt operator, used for sorting
	User(int id = -1,							//	constructor
		time_t birthday = -1,
		Gender gender = Gender::UNKNOWN,
		int city_id = -1,
		time_t time_reg = -1);
};

