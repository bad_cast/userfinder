#include "User.h"

using namespace std;

bool User::operator==(const User& other) const
{
	bool b_id = (other.id == -1) || (other.id == id);
	bool b_birth = (other.birthday == -1) || (other.birthday == birthday);
	bool b_gender = (other.gender == Gender::UNKNOWN) || (other.gender == gender);
	bool b_city = (other.city_id == -1) || (other.city_id == city_id);
	bool b_time_reg = (other.time_reg == -1) || (other.time_reg == time_reg);
	
	return b_id && b_birth && b_gender && b_city && b_time_reg;
}

bool User::operator<(const User& other) const {
	return (time_reg < other.time_reg);
}

User::User(int id, time_t birthday, Gender gender, int city_id, time_t time_reg) : 
	id(id),
	birthday(birthday),
	gender(gender),
	city_id(city_id),
	time_reg(time_reg) {
}
