#pragma once
#include <ctime>

class UserShort
{
public:
	int idx;			// index
	time_t time_reg;	// reg date

	bool operator<(const UserShort& other) const;	// lt operator, used for sorting
	UserShort(int idx = -1, time_t time_reg = -1);	// default constructor
};

