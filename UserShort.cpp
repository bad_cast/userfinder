#include "UserShort.h"

bool UserShort::operator<(const UserShort& other) const {
	return (time_reg < other.time_reg);
}

UserShort::UserShort(int idx, time_t time_reg) :
	idx(idx),
	time_reg(time_reg) {
}
