﻿#include <string>
#include <cstdio>
#include <chrono>
#include <ctime>
#include <conio.h>
#include "UserManager.h"

constexpr auto MAX_USERS = (50 * 1000 * 1000);	//	maximum users in base
constexpr auto NUM_USERS = (10 * 1000 * 1000);	//	default number of users in base

constexpr auto KEY_LEFT = 75;					//	code for arrow left
constexpr auto KEY_RIGHT = 77;					//	code for arrow right
constexpr auto KEY_ESC = 27;					//	code for ESCAPE

/* get diff between two std counters in msecs */
std::chrono::milliseconds get_diff(std::chrono::time_point<std::chrono::system_clock> start) {
	auto diff = std::chrono::system_clock::now() - start;
	auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
	return msec;
}

/*	read long integer from std::cin in given range 
	tries again if not numeric
	type -1 to skip 
	return long integer */
long read_number(std::string message, long left = -1, long right = -1) {
	int x = -1;
	if (message != "") {
		message += " (enter -1 to skip parameter)";
		message += " [";
		if (left != -1) {
			message += std::to_string(left);
		}
		message += " - ";
		if (right != -1) {
			message += std::to_string(right);
		}
		message += "]: ";
		std::cout << message;
	}
	while (!(std::cin >> x) || 
			(left != -1 && x != -1 && x < left) ||
			(right != -1 && x != -1 && x > right)) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Invalid input.  Try again: ";
	}
	return x;
}

/*	read date from cin year, month and day of month after each other
	return time_t
*/
time_t read_date(std::string event) {
	tm t = { 0 };
	time_t tt = -1;
	t.tm_year = read_number(std::string("Enter " + event + " year"), 1900, 2037);
	if (t.tm_year != -1) {
		t.tm_year -= 1900;
		t.tm_mon = read_number(std::string("Enter " + event + " month"), 1, 12);
		if (t.tm_mon != -1) {
			t.tm_mon--;
			t.tm_mday = read_number(std::string("Enter " + event + " day of month"), 1, 31);
		}
	}
	time_t rawtime = time(NULL);
	struct tm ptm = {0};
	gmtime_s(&ptm, &rawtime);
	time_t gmt = mktime(&ptm);
	localtime_s(&ptm, &rawtime);
	time_t offset = rawtime - gmt + (ptm.tm_isdst ? 3600 : 0);

	if (t.tm_year != -1 && t.tm_mon != -1 && t.tm_mday != -1) {
		tt = mktime(&t);
		tt += offset;
		if (tt == -1) {
			std::cout << "Incorrect date. Skipping." << std::endl;
		}
	}
	return tt;
}

/* print help message */
void print_main_menu() {
	std::cout << "===========================================================================" << std::endl;
	std::cout << "1. Generate base" << std::endl;
	std::cout << "2. Save base" << std::endl;
	std::cout << "3. Load base" << std::endl;
	std::cout << "4. Search user by parameters" << std::endl;
	std::cout << "5. Set limit for one page" << std::endl;
	std::cout << "6. Add user" << std::endl;
	std::cout << "ESC. Exit" << std::endl;
	std::cout << "===========================================================================" << std::endl;
}

/* print hello message */
void print_hello(void) {
	std::cout << "User Finder ver.0.01." << std::endl;
	print_main_menu();
}

/* print navigation help message after successful search */
void print_left_right(void) {
	std::cout << "===========================================================================" << std::endl;
	std::cout << "Press left or right arrow to change page or ESC to exit." << std::endl;
	std::cout << "===========================================================================" << std::endl;
}

/* main section. all navigation logic is here */
int main()
{
	UserManager manager;
	Users users;
	User user;
	auto start = std::chrono::system_clock::now();
	std::chrono::milliseconds msec;
	unsigned page_idx = 0;

	print_hello();

	while (1) {
		switch (_getch()) {
		case '1': {
			int n_users;
			n_users = read_number("Enter user number", 1, MAX_USERS);
			if (n_users == -1) {
				n_users = NUM_USERS;
			}
			start = std::chrono::system_clock::now();
			manager.GenerateUsers(n_users, true);
			msec = get_diff(start);
			std::cout << "Users base generated. Generation time " << msec.count() << 
				" msec. Users in base: "<< manager.GetUsersCount() << std::endl;
		}
		break;

		case '2': {
			start = std::chrono::system_clock::now();
			if (manager.SaveUsers()) {
				msec = get_diff(start);
				std::cout << "Users base saved to users.dat. Save time: " << 
					msec.count() << " msec." << std::endl;
			}
			else {
				std::cout << "Saving file failed for some reason." << std::endl;
			}
		}
		break;

		case '3': {
			start = std::chrono::system_clock::now();
			if (manager.LoadUsers()) {
				msec = get_diff(start);
				std::cout << "Users loaded from users.dat. Loading time: " << msec.count() << 
					" msec. Users in base: " << manager.GetUsersCount() << std::endl;
			}
			else {
				std::cout << "File not found." << std::endl;
			}
		}
		break;

		case '4': {
			if (!manager.GetUsersCount()) {
				std::cout << "Base is empty. Skipping." << std::endl;
				break;

			}
			page_idx = 0;
			user.id = read_number(std::string("Enter id"), 0, -1);
			user.birthday = read_date("birthday");
			user.gender = read_number(std::string("Enter gender"), 0, Gender::LAST - 1);
			user.city_id = read_number(std::string("Enter city id"), 0, 1000);
			user.time_reg = read_date("reg");

			start = std::chrono::system_clock::now();
			auto n_users = manager.FindUsers(user);
			msec = get_diff(start);
			std::cout << "Search completed. Found " << n_users << " users in " <<
				msec.count() << " msec." << std::endl;
			start = std::chrono::system_clock::now();
			manager.SortFoundUsersByTimeReg();
			msec = get_diff(start);
			std::cout << "Sort completed in " << 
				msec.count() << " msec." << std::endl;
			if (!manager.GetPagesNumber()) {
				print_main_menu();
				break;
			}
			manager.PrintPage(0);
			print_left_right();

			while (int c = _getch()) {
				switch (c) {
				case KEY_RIGHT:
					if (!manager.GetPagesNumber())
						break;
					if (page_idx < manager.GetPagesNumber() - 1) {
						page_idx++;
						manager.PrintPage(page_idx);
					}
					break;

				case KEY_LEFT:
					if (!manager.GetPagesNumber())
						break;
					if (page_idx > 0) {
						page_idx--;
						manager.PrintPage(page_idx);
					}
					break;
				}
				if (c == KEY_ESC) {
					print_main_menu();
					break;
				}
			}
		}
		break;

		case '5': {
			unsigned limit = read_number(std::string("Enter limit for one page"), 1, 1000);
			if (limit == -1) {
				std::cout << "Limit was not set. Now is " << manager.GetPageLimit() << " per page." << std::endl;
				break;
			}
			manager.SetPageLimit(limit);
			page_idx = 0;
			std::cout << "Limit set to " << limit << std::endl;
		}
		break;

		case '6': {
			User user;
			tm t = { 0 };
			user.id = (int)manager.GetUsersCount();
			user.birthday = read_date("birthday");
			user.gender = read_number(std::string("Enter gender"), 0, Gender::LAST - 1);
			user.city_id = read_number(std::string("Enter city id"), 0, 1000);
			user.time_reg = read_date("reg");
			manager.AddUser(&user);
			std::cout << "User added. Base size is " << manager.GetUsersCount() << std::endl;
		}
		break;

		case KEY_ESC:
			return 0;
		}
	}

	return 0;
}