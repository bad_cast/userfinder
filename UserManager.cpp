#include "UserManager.h"


UserManager::UserManager(const std::string& filename): 
	m_filename(filename),
	m_limit(20) {
}

void UserManager::GenerateUser(int id, User* user)
{
	constexpr auto LEFT_BORDER_BIRTHDAY = (0 - 50 * 365);
	constexpr auto LEFT_BORDER_REG = (35 * 365);
	constexpr auto MAX_CITY_ID = (1000);
	constexpr auto SECONDS_IN_DAY = (24 * 60 * 60);

	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::uniform_int_distribution<time_t> d_birth(LEFT_BORDER_BIRTHDAY, (int)(now / SECONDS_IN_DAY));
	std::uniform_int_distribution<time_t> d_reg(LEFT_BORDER_REG, (int)(now / SECONDS_IN_DAY));
	std::uniform_int_distribution<int> d_city(0, MAX_CITY_ID);
	std::uniform_int_distribution<int> d_gender(0, Gender::LAST - 1);
	
	user->id = id;
	user->birthday = d_birth(m_generator) * SECONDS_IN_DAY;
	user->time_reg = d_reg(m_generator) * SECONDS_IN_DAY;
	user->city_id = d_city(m_generator);
	user->gender = d_gender(m_generator);
}

void UserManager::GenerateUsers(int n_users, bool clear)
{
	if (clear) {
		m_users.clear();
	}
	for (int i = 0; i < n_users; i++) {
		User user;
		GenerateUser(i, &user);
		m_users.push_back(user);
	}

}

bool UserManager::SaveUsers(void)
{
	std::ofstream outfile(m_filename, std::ofstream::binary);
	if (!outfile)
		return false;
	for (const auto &user : m_users)	{
		outfile.write(reinterpret_cast<const char*>(&user), sizeof(user));
	}
	outfile.close();
	return true;
}

bool UserManager::LoadUsers(void)
{
	User user;
	if (m_filename == "")
		return false;
	std::ifstream infile(m_filename, std::ofstream::binary);
	if (!infile)
		return false;
	m_users.clear();
	infile.seekg(0, infile.end);
	int len = (int)(infile.tellg());
	infile.seekg(0, infile.beg);
	int pos = 0;
	while ((pos < len) && ((len - pos) >= sizeof(user))) {
		infile.read(reinterpret_cast<char*>(&user), sizeof(user));
		m_users.push_back(user);
		pos += sizeof(user);
	}
	
	return true;
}

size_t UserManager::FindUsers(const User& user) {
	ClearFoundUsers();
	
	for (auto it = m_users.begin(); it != m_users.end(); it++) {
		if (*it == user) {
			m_ids.push_back(UserShort((int)std::distance(m_users.begin(), it), it->time_reg));
		}
	}
	return m_ids.size();
}

size_t UserManager::GetUsersCount(void) {
	return m_users.size();
}

size_t UserManager::GetFoundUsers(Users* users) {
	users->clear();
	for (const auto& user_short : m_ids) {
		users->push_back(m_users[user_short.idx]);
	}
	return users->size();
}

size_t UserManager::GetFoundUsersCount(void) {
	return m_ids.size();
}

size_t UserManager::GetPage(Users* users, unsigned page) {
	unsigned counter = 0;
	if (m_ids.size() <= m_limit * page) {
		return -1;
	}
	users->clear();

	auto it = std::next(m_ids.begin(), m_limit * page);


	for (; it != m_ids.end(); it++) {
		users->push_back(m_users[it->idx]);
		if (users->size() >= m_limit) {
			break;
		}
	}
	return users->size();
}

size_t UserManager::GetPagesNumber(void) {
	return size_t(m_ids.size() / m_limit) + ((m_ids.size() % m_limit) ? 1 : 0);
}

void UserManager::ClearFoundUsers(void)
{
	m_ids.clear();
}

void UserManager::SetPageLimit(unsigned limit) {
	m_limit = limit;
}

size_t UserManager::GetPageLimit(void) {
	return m_limit;
}

size_t UserManager::PrintFoundUsers(void) {
	Users users;
	GetFoundUsers(&users);
	for (const auto& user : users) {
		PrintUser(&user);
	}
	return users.size();
}

size_t UserManager::PrintPage(unsigned page)
{
	Users users;
	GetPage(&users, page);

	std::cout << std::setw(15) << std::right << "User" <<
		std::setw(15) << std::right << "Birthday" <<
		std::setw(15) << std::right << "Gender" <<
		std::setw(15) << std::right << "City ID" <<
		std::setw(15) << std::right << "Time Reg" << std::endl;

	std::cout << "===========================================================================" << std::endl;

	for (const auto& user : users) {
		PrintUser(&user);
	}
	std::cout << "========================= Page " << 
		page+1 << " of " << GetPagesNumber() << " ===================================" << std::endl;
	return users.size();
}

void UserManager::PrintUser(const User* user)
{
	struct tm t_b = { 0 };
	struct tm t_r = { 0 };
	gmtime_s(&t_b, &user->birthday);
	gmtime_s(&t_r, &user->time_reg);
	std::cout << std::setw(15) << std::right << user->id << "       " <<
		std::right << t_b.tm_year + 1900 << "." << 
		std::setw(2) << std::right << std::setfill('0') << t_b.tm_mon + 1 << "." <<
		std::setw(2) << std::right << t_b.tm_mday << std::setfill(' ') <<
		std::setw(15) << std::right << user->gender <<
		std::setw(15) << std::right << user->city_id << "       " <<
		std::right << t_r.tm_year + 1900 << "." <<
		std::setw(2) << std::right << std::setfill('0') << t_r.tm_mon + 1 << "." <<
		std::setw(2) << std::right << t_r.tm_mday << std::setfill(' ') << std::endl;
}

bool UserManager::AddUser(const User* user)
{
	m_users.push_back(*user);
	return true;
}

void UserManager::SortFoundUsersByTimeReg(void) {
	std::sort(m_ids.begin(), m_ids.end());
}

void UserManager::SortAllUsersByTimeReg(void) {
	std::sort(m_users.begin(), m_users.end());
}


